__author__ = 'Administrator'
#coding=utf-8
from sklearn import svm
from sklearn.ensemble import RandomForestClassifier
from sklearn import linear_model
from sklearn.linear_model import LogisticRegression
from sklearn import preprocessing
from sklearn.ensemble import GradientBoostingClassifier, GradientBoostingRegressor
import  time
#from sklearn.linear_model import LogisticRegression


train_filename = "input/QID2 Feature Train_112NUM_GBRTNULL.dat"
predict_filename = "input/QID2 Feature Predict_112NUM_GBRTNULL.dat"
file_result = open("result/SVMRank(rbf)_Qid2_f110_gbrtnull_sem3_book_15_30_60_120_library_15_30_60_120_card60_120.txt","w")
SUBMIT = 1
n_feature = 0
predict_line_id = []
predict_line_to = []

def Read_Train_Data(file_name):
    file = open(file_name)
    x = []
    y = []
    first_line = 0
    positive = 0
    nagative = 0
    for line in file:
        if len(line) < 0:
            continue
        if first_line == 0:
            first_line = 1
            continue
        line_cur = line.strip().split(',')
        x.append([float(line_cur[i]) for i in range(2,len(line_cur)-1)])
        #print line_cur[len(line_cur)-1]
        y.append(float(line_cur[len(line_cur)-1]))
        if int(line_cur[len(line_cur)-1]) == 1:
            positive += 1
        else:
            nagative += 1
        if positive + nagative >= 30000:
            break
    print "正样本:" + str(positive) + "负样本:" + str(nagative)
    #print "x,"
    #print x
    return x,y

def Read_Predict_Data(file_name):
    global predict_line_id
    global predict_line_to
    file = open(file_name)
    x = []
    first_line = 0
    for line in file:
        if len(line) < 0:
            continue
        line_cur = line.strip().split(',')
        if first_line == 0:
            global n_feature
            n_feature = len(line_cur)-2
            print "特征总数："+str(len(line_cur)-2)
            first_line = 1
            continue

        x.append([float(line_cur[i]) for i in range(2,len(line_cur))])
        predict_line_id.append(int(line_cur[0]))
        predict_line_to.append(int(line_cur[1]))
    #print "x,"
    #print x
    return x

def Get_Rank(res):
    #LR
    out_dict = {}
    for i in range(0,len(predict_line_id)):
        out_dict.setdefault(predict_line_id[i],0)
        out_dict[predict_line_id[i]] += res[i]
    # print out_dict
    # file_preresult.write("id,rank\n")
    # for idx in out_dict:
    #     file_preresult.write(str(idx)+","+str(92-int(out_dict[idx]))+"\n")
    rank = {}
    cnt = 0
    for id in range(1,92):
        if id not in out_dict:
            rank[id] = 92.0
        else:
            cnt += 1
            rank[id] = 92.0 - out_dict[id]

    I = []
    for i in rank:
        I.append((i,rank[i]))
    print "*******Before Ranked********"
    print I
    I.sort(lambda a,b :cmp(a[1],b[1]))
    print "*******Ranked********"
    print I
    #print I
    rank = {}
    for i in range(0,91):
        rank[I[i][0]] = i + 1
    return rank

def PreProcess(x):
    ###标准化
    #stand_x = preprocessing.scale(x)
    min_max_scaler = preprocessing.MinMaxScaler()
    minmax_x = min_max_scaler.fit_transform(x)
    return minmax_x;

def Test(res,file):
    error = 0.0
    first_line = 0
    file.seek(0)
    for line in file:
        if first_line == 0:
            first_line = 1
            continue
        line_cur = line.split(",")
        id = int(line_cur[0])
        rank = int(line_cur[1])
        error += pow(rank-res[id],2) * 6
    error = 1 - error * 1.0 / ((91 * 91 - 1) * 91)
    return error

if __name__ == "__main__":
    print time.localtime()
    x,y = Read_Train_Data(train_filename)
    #print y
    pre_x = Read_Predict_Data(predict_filename)
    x = PreProcess(x)
    pre_x = PreProcess(pre_x)
    clf = svm.SVC()
    #clf = svm.LinearSVC()
    clf.fit(x,y)

    res  = clf.predict(pre_x)
    #res = clf.predict_proba(pre_x)
    print len(res)
    out_dict = Get_Rank(res)
    #print out_dict
    if SUBMIT == 1:
        test_dict = {}
        file_result.write("id,rank\n")
        for idx in out_dict:
            test_dict[idx] = 92-int(out_dict[idx])
            file_result.write(str(idx)+","+str(test_dict[idx])+"\n")
    print time.localtime()