# -#- coding=utf-8 -*-
from sklearn import svm
from sklearn.ensemble import RandomForestClassifier
from sklearn import linear_model
from sklearn.linear_model import LogisticRegression
from sklearn import preprocessing
from sklearn.ensemble import GradientBoostingClassifier, GradientBoostingRegressor
import  time
import math


train_filename = "input/QID2 Feature Train_112NUM_GBRTNULL.dat"
predict_filename = "input/QID2 Feature Predict_112NUM_GBRTNULL.dat"
file_result_name = "result/AdaBoosting_f112_sem3_"
SUBMIT = 1
n_feature = 0
n_sample = 0
model_weight = []
sample_weight = []
model_list = []
predict_line_id = []
predict_line_to = []

def Read_Train_Data(file_name):
    file = open(file_name)
    x = []
    y = []
    first_line = 0
    positive = 0
    nagative = 0
    for line in file:
        if len(line) < 0:
            continue
        if first_line == 0:
            first_line = 1
            continue
        line_cur = line.strip().split(',')
        x.append([float(line_cur[i]) for i in range(2,len(line_cur)-1)])
        y.append(float(line_cur[len(line_cur)-1]))
        if int(line_cur[len(line_cur)-1]) == 1:
            positive += 1
        else:
            nagative += 1
        if positive + nagative >= 30000:
            break
    global n_sample
    n_sample = positive + nagative
    print "正样本:" + str(positive) + "负样本:" + str(nagative)
    #print "x,"
    #print x
    return x,y

def Read_Predict_Data(file_name):
    global predict_line_id
    global predict_line_to
    file = open(file_name)
    x = []
    first_line = 0
    for line in file:
        if len(line) < 0:
            continue
        line_cur = line.strip().split(',')
        if first_line == 0:
            global n_feature
            n_feature = len(line_cur)-2
            print "特征总数："+str(len(line_cur)-2)
            first_line = 1
            continue

        x.append([float(line_cur[i]) for i in range(2,len(line_cur))])
        predict_line_id.append(int(line_cur[0]))
        predict_line_to.append(int(line_cur[1]))
    #print "x,"
    #print x
    return x

def Get_Rank(res):
    #LR
    out_dict = {}
    print res
    for i in range(0,len(predict_line_id)):
        out_dict.setdefault(predict_line_id[i],0)
        out_dict[predict_line_id[i]] += res[i]
    # print out_dict
    # file_preresult.write("id,rank\n")
    # for idx in out_dict:
    #     file_preresult.write(str(idx)+","+str(92-int(out_dict[idx]))+"\n")
    rank = {}
    cnt = 0
    for id in range(1,92):
        if id not in out_dict:
            rank[id] = 92.0
        else:
            cnt += 1
            rank[id] = 92.0 - out_dict[id]
    print cnt

    I = []
    for i in rank:
        I.append((i,rank[i]))
    print I
    I.sort(lambda a,b :cmp(a[1],b[1]))
    print "*******Ranked********"
    print I
    #print I
    rank = {}
    for i in range(0,91):
        rank[I[i][0]] = i + 1
    return rank

def PreProcess(x):
    ###标准化
    #stand_x = preprocessing.scale(x)
    min_max_scaler = preprocessing.MinMaxScaler()
    minmax_x = min_max_scaler.fit_transform(x)
    return minmax_x;

def Test(res,file):
    error = 0.0
    first_line = 0
    file.seek(0)
    for line in file:
        if first_line == 0:
            first_line = 1
            continue
        line_cur = line.split(",")
        id = int(line_cur[0])
        rank = int(line_cur[1])
        error += pow(rank-res[id],2) * 6
    error = 1 - error * 1.0 / ((91 * 91 - 1) * 91)
    return error
def Train_Model(x,y,t):
    if t == 0:
        clf =  clf =  LogisticRegression(C=0.0093, penalty='l1', tol=0.0005)
        clf.fit(x,y)
        model_list.append(clf)
        res = clf.predict(x)
        return res
    elif t == 1:
        clf =  LogisticRegression(C=0.0055, penalty='l1', tol=0.0005)
        clf.fit(x,y)
        model_list.append(clf)
        res = clf.predict(x)
        return res
    elif t == 2:
        clf =  LogisticRegression(C=0.013, penalty='l1', tol=0.0005)
        clf.fit(x,y)
        model_list.append(clf)
        res = clf.predict(x)
        return res
    elif t == 3:
        clf =  svm.SVC()
        clf.fit(x,y)
        model_list.append(clf)
        res = clf.predict(x)
        return res
    elif t == 4:
        clf =  GradientBoostingClassifier(n_estimators=3000, max_depth=6)
        clf.fit(x,y)
        model_list.append(clf)
        res = clf.predict(x)
        return res
    elif t == 5:
        clf =  GradientBoostingClassifier(n_estimators=40, max_depth=6)
        clf.fit(x,y)
        model_list.append(clf)
        res = clf.predict(x)
        return res
    elif t == 6:
        clf =  GradientBoostingClassifier(n_estimators=35, max_depth=6)
        clf.fit(x,y)
        model_list.append(clf)
        res = clf.predict(x)
        return res
    elif t == 7:
        clf = GradientBoostingClassifier(n_estimators=1037, max_depth=6)
        clf.fit(x,y)
        model_list.append(clf)
        res = clf.predict(x)
        return res
    elif t == 8:
        clf =  LogisticRegression(C=0.0073, penalty='l1', tol=0.0005)
        clf.fit(x,y)
        model_list.append(clf)
        res = clf.predict(x)
        return res
    elif t == 9:
        clf =  LogisticRegression(C=0.0068, penalty='l1', tol=0.0006)
        clf.fit(x,y)
        model_list.append(clf)
        res = clf.predict(x)
        return res

if __name__ == "__main__":
    print time.localtime()
    x,y = Read_Train_Data(train_filename)
    #print y
    pre_x = Read_Predict_Data(predict_filename)
    x = PreProcess(x)
    pre_x = PreProcess(pre_x)
    for i in range(0,n_sample):
        sample_weight.append(1.0/n_sample)

    for t in range(0,10):#10个模型
        print "this t = " + str(t)
        train_y = Train_Model(x,y,t)
        #print train_y
        #print y
        error = 0.0
        for i in range(0,len(y)):#计算分类错误率
            #print len(x),len(y),len(sample_weight)
            error += 0.5 * sample_weight[i] * abs(train_y[i] - y[i])
        print error
        if error == 0:
            model_weight.append(100)
        else:
            model_weight.append(math.log((1-error)/error))
        sum = 0.0
        for i in range(0,n_sample):
            if train_y[i] != y[i]:
                if error == 0:
                    sample_weight[i] *= (1 - error) / error
                else:
                    sample_weight[i] *= 10000
            sum += sample_weight[i]
        for i in range(0,n_sample):
            sample_weight[i] = sample_weight[i] / sum

    adares = []
    print model_weight
    temp = {}
    for t in range(0,10):
        for j in range(1,1000):
            res  = model_list[t].predict(pre_x)
            # print "current res:",
            # print res
            for i in range(0,len(pre_x)):

                if res[i] < 0.5:
                    res[i] = -1
                if t == 0:
                    adares.append(res[i]*model_weight[t])
                else:
                    adares[i] += res[i]*model_weight[t]
            temp.setdefault(j,{})
            temp[j] = res
        # print "after process res:",
        # print res
    #res = clf.predict_proba(pre_x)
    for i in range(0,len(pre_x)):
        if adares[i] <= 0:
            adares[i] = 0
        else:
            adares[i] = 1
    print len(adares)
    out_dict = Get_Rank(adares)
    #print out_dict
    if SUBMIT == 1:
        file_result_name += "m10" + time.strftime('%mm%dd%Hh%Mm%Ss') + ".txt"
        print file_result_name
        file_result = open(file_result_name,"w")
        test_dict = {}
        file_result.write("id,rank\n")
        for idx in out_dict:
            test_dict[idx] = 92-int(out_dict[idx])
            file_result.write(str(idx)+","+str(test_dict[idx])+"\n")
    print time.localtime()