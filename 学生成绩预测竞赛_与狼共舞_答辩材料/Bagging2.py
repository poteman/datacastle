__author__ = 'Administrator'
#coding=utf-8

WRITE = 1
Weights = [0.5,0.5]
file_score1 = open("result/BUG_ListNet_f55_step0.09_ite130000_sem3_book_7_15_60_120_library_7_15_60_120_card60_120.txt")
file_score2 = open("result/LassoRank(0018)_f110_gbrtnull_sem3_book_7_15_60_120_library_7_15_60_120_card60_120.txt")
file_score_write = open("result/bagging_(ListNet)X0"+str(Weights[0])+"_LassoX0"+str(Weights[1])+".txt","w")

def Read_Score(file_score):
    first_line = 0
    score = {}
    for line in file_score:
        if first_line == 0:
            first_line = 1
            continue
        line_cur = line.strip().split(",")
        score[line_cur[0]] = float(line_cur[1])
    return score

def Merge(s1,s2,x1,x2):
    new_score = {}
    for stu in s1:
        if stu in s2:
            new_score[stu] = x1 * s1[stu] + x2 * s2[stu]
        else:
            new_score[stu] = s1[stu]
    return new_score

def Test(res,file):
    error = 0.0
    first_line = 0
    file.seek(0)
    for line in file:
        print line
        if first_line == 0:
            first_line = 1
            continue
        line_cur = line.split(",")
        id = int(line_cur[0])
        rank = int(line_cur[1])
        error += pow(rank-res[id],2) * 6
    error = 1 - error * 1.0 / ((91 * 91 - 1) * 91)
    return error

if __name__ == "__main__":
    score1 = Read_Score(file_score1)
    score2 = Read_Score(file_score2)
    score = Merge(score1,score2,Weights[0],Weights[1])
    print score1
    print score2
    print score
    if WRITE == 1:
        I = []
        for i in score:
            I.append((i,score[i]))
        I.sort(lambda a,b :cmp(a[1],b[1]))
        #print I
        rank = {}
        for i in range(0,91):
            rank[I[i][0]] = i + 1
        file_score_write.write("id,rank\n")
        #print len(rank)
        for stu in rank:
            file_score_write.write(str(stu)+","+str(rank[stu])+'\n')
        print "rank"
        print rank