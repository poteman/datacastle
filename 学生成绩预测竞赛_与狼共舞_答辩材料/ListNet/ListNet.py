__author__ = 'Administrator'
#coding=utf-8
from sklearn import svm
from sklearn.ensemble import RandomForestClassifier
from sklearn import linear_model
from sklearn.linear_model import LogisticRegression
from sklearn import preprocessing
import  time
import math
#from sklearn.linear_model import LogisticRegression


train_filename = "input/ListNet Feature Train_FromPre_50NUM.dat"
predict_filename = "input/ListNet Feature Predict_FromPre_50NUM.dat"

SUBMIT = 1
INIT_TYPE = 1
predict_line_id = []
predict_line_to = []
init_weights = 0#��ʼȨ��
iterate_num = 120000#ѵ����"?��
Step = 0.09#ѧϰ����
file_result = open("result/ListNet_f55_step"+str(Step)+"_ite"+str(iterate_num)+"_sem3_book_7_15_60_120_library_7_15_60_120_card60_120.txt","w")
file_preresult = open("result/ListNet_UnRank_f55_step"+str(Step)+"_ite"+str(iterate_num)+"_sem3_book_7_15_60_120_library_7_15_60_120_card60_120.txt","w")
file_initweight = open("listnet_init_f55_step0.09_ite120000_03m16d11h50m55s.txt")

def Read_Train_Data(file_name):
    file = open(file_name)
    x = []
    y = []
    first_line = 0
    for line in file:
        if len(line) < 0:
            continue
        # if first_line == 0:
        #     first_line = 1
        #     continue
        line_cur = line.strip().split(',')
        x.append([float(line_cur[i]) for i in range(2,len(line_cur)-1)])
        y.append(float(line_cur[len(line_cur)-1])/91.0)
    #print "x,"
    #print x
    return x,y
def Read_Predict_Data(file_name):
    global predict_line_id
    global predict_line_to
    file = open(file_name)
    x = []
    first_line = 0
    for line in file:
        if len(line) < 0:
            continue
        line_cur = line.strip().split(',')
        if first_line == 0:
            print "����������"+str(len(line_cur)-2)
            first_line = 1
            #continue

        x.append([float(line_cur[i]) for i in range(2,len(line_cur))])
        predict_line_id.append(float(line_cur[0]))
        predict_line_to.append(float(line_cur[1]))
    #print "x,"
    #print x
    return x
def PreProcess(x):
    ###��׼��
    #stand_x = preprocessing.scale(x)
    ###��һ������1��
    # for i in range(0,len(x)):
    #     x[i].append(1.0)
    min_max_scaler = preprocessing.MinMaxScaler()
    minmax_x = min_max_scaler.fit_transform(x)
    return minmax_x;
def DotMultiply(x,y):
    dotsum = 0.0
    for i in range(0,len(x)):
        dotsum += x[i] * y[i]
    return dotsum
def Init_ListNet(f_num):
    warray = []
    if INIT_TYPE == 1:
        for line in file_initweight:
            line_cur  = line.split(",")
            for i in line_cur:
                warray.append(float(i))
        global iterate_num
        iterate_num = 1
    else:
        for i in range(0,f_num):
            warray.append(0.0)
    return warray
def ListNet(x,y,pre_x):
    #####
    ##Train
    #####
    weights = Init_ListNet(len(x[0]))
    for i in range(0,iterate_num):
        doclist = []
        Z = []
        ExpYList = []
        ExpYSum = 0.0
        oldweights = [weights[v] for v in range(0,len(weights))]
        for doc in range(0,len(x)):
            Z.append(DotMultiply(weights,x[doc]))
            ExpYList.append(math.exp(y[doc]))
            ExpYSum += ExpYList[doc]
        for v in range(0,len(x[0])):
            deltaWP1 = 0.0
            deltaWP2 = 0.0
            deltaWP3 = 0.0
            for doc in range(0,len(x)):
                deltaWP1 -= ExpYList[doc]*x[doc][v]
                expz = math.exp(Z[doc])
                deltaWP2 += expz*x[doc][v]
                deltaWP3 += expz
            deltaW = deltaWP1/ExpYSum + deltaWP2/deltaWP3;
            weights[v] -=Step * deltaW
        sum = 0.0
        for v in range(0,len(x[0])):
            sum += math.pow(oldweights[v]-weights[v],2)
        # print "finish training " + str(i+1) + "/" + str(iterate_num)+ " �����ǣ�" + str(sum)
        # for v in range(0,len(x[0])):
        #     print "weight[" + str(v) + "] = " + str(weights[v])
    file_model = "model/model_listnet_f55_step"+str(Step)+"_ite"+str(iterate_num)+ "_"+time.strftime('%mm%dd%Hh%Mm%Ss') + str(".txt")
    write_model = open(file_model,"w")
    for i in range(0,len(weights)):
        if i  == 0:
             write_model.write(str(weights[i]))
        else:
             write_model.write(","+str(weights[i]))
    #####
    ##Predict
    #####
    res = []
    for i in range(0,len(pre_x)):
        res.append(DotMultiply(pre_x[i],weights))
    return res


def Get_Rank(res):
    #LR
    out_dict = {}
    for i in range(0,len(predict_line_id)):
        out_dict.setdefault(predict_line_id[i],0)
        out_dict[predict_line_id[i]] += res[i]
    file_preresult.write("id,rank\n")
    for idx in out_dict:
        file_preresult.write(str(idx)+","+str(92-int(out_dict[idx]))+"\n")
    rank = {}
    cnt = 0
    for id in range(1,92):
        if id not in out_dict:
            rank[id] = 92.0
        else:
            cnt += 1
            rank[id] = 92.0 - out_dict[id]
    print cnt

    I = []
    for i in rank:
        I.append((i,rank[i]))
    print I
    I.sort(lambda a,b :cmp(a[1],b[1]))
    print "*******Sorted********"
    print I
    #print I
    rank = {}
    for i in range(0,91):
        rank[I[i][0]] = i + 1
    return rank

def Test(res,file):
    error = 0.0
    first_line = 0
    file.seek(0)
    for line in file:
        if first_line == 0:
            first_line = 1
            continue
        line_cur = line.split(",")
        id = int(line_cur[0])
        rank = int(line_cur[1])
        error += pow(rank-res[id],2) * 6
    error = 1 - error * 1.0 / ((91 * 91 - 1) * 91)
    return error

if __name__ == "__main__":
    print time.localtime()
    x,y = Read_Train_Data(train_filename)
    print len(x)
    #print y
    pre_x = Read_Predict_Data(predict_filename)

    #######
    ###Ԥ����
    #######
    x = PreProcess(x)
    pre_x = PreProcess(pre_x)
    print len(pre_x)

    res  = ListNet(x,y,pre_x)
    #res = clf.predict_proba(pre_x)
    print "len of res:" + str(len(res))
    print res
    out_dict = Get_Rank(res)
    #print out_dict
    if SUBMIT == 1:
        test_dict = {}
        file_result.write("id,rank\n")
        for idx in out_dict:
            test_dict[idx] = 92-int(out_dict[idx])
            file_result.write(str(idx)+","+str(test_dict[idx])+"\n")
    print time.localtime()