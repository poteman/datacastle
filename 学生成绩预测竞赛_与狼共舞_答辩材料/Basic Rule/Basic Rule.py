# -*- coding:utf-8 -*-
def Weight_score():
    file_score = open("input/Score Predict.csv")
    file_result = open("result/35 and 65 of first sem rank.txt","w")
    SUBMIT = 1
    factor = [0.35,0.65]
    stu = {}
    first_line = 0
    for line in file_score:
        if first_line == 0:
            first_line = 1
            continue
        sem,id,rank = line.strip("").split(",")
        stu.setdefault(id,0)
        stu[id] += int(rank) * factor[int(sem)-1]
    #for idx in stu:
    #    stu[idx] /= 2

    if SUBMIT == 1:
        file_result.write("id,rank\n")
        for idx in stu:
            file_result.write(str(int(idx))+","+str(stu[idx])+"\n")

def var_based_score():
    file_score = open("input/Score Predict.csv")
    file_result = open("result/Mean Of The first Two Sem.txt","w")
    SUBMIT = 1
    factor = [0.5,0.5]
    stu = {}
    score = {}
    first_line = 0
    for line in file_score:
        if first_line == 0:
            first_line = 1
            continue
        sem,id,rank = line.strip("").split(",")
        stu.setdefault(id,[0])
        stu[id].append(int(rank))
    print stu
    Temp = []
    for id in stu:
        mid = (stu[id][1] + stu[id][2])/2
        var = (stu[id][1]-mid)**2 + (stu[id][2]-mid)**2
        w = stu[id][1] * factor[0] + factor[1] * stu[id][2]
        #print (id,stu[id][1],stu[id][2],mid,var,w)
        Temp.append((id,stu[id][1],stu[id][2],mid,var,w))
    Temp.sort(lambda a,b:cmp(a[4],b[4]))
    Unsorted = []
    rank_hash = {}
    for Temp_id in Temp:
        # print Temp_id[0],Temp_id[1],Temp_id[2]
        if Temp_id[4] <= 50:
            score[Temp_id[0]] = Temp_id[3]#方差小取第二学期
            rank_hash.setdefault(score[Temp_id[0]],1)
        # else:
        #     Unsorted.append(Temp_id)
        Unsorted.append(Temp_id)
    print Unsorted
    Unsorted.sort(lambda a,b:cmp(a[5],b[5]))#方差大按权重排
    print Unsorted
    cnt = 1
    for Unsorted_id in Unsorted:
        # while cnt in rank_hash:
        #     cnt += 1
        #     continue
        rank_hash.setdefault(cnt,1)
        score[Unsorted_id[0]] = cnt
        cnt+=1
    print score
    ##验证数据适合合法
    check = {}
    for idx in score:
        check.setdefault(score[idx],0)
    if len(score) != 91 or len(check) != 91:
        print "提交结果不合法"
        print "*****************************************************************************"
        return
    if SUBMIT == 1:
        file_result.write("id,rank\n")
        for idx in score:
            file_result.write(str(int(idx))+","+str(score[idx])+"\n")

if __name__ == '__main__':
    Weight_score()#第一二学期排名乘以权重
    #var_based_score()#两学期排名相差大和小分开处理
    Dict = {'b':2,'a':4,'c':3}
    Dict = sorted(Dict.items(),key=lambda t:t[0],reverse=False)# key=lambda t:t[0] 按key排序,t[1]按value排序
    print Dict
