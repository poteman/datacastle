__author__ = 'Administrator'
#coding:utf-8
import time
import datetime
from sklearn import svm
from sklearn.ensemble import RandomForestClassifier
from sklearn import linear_model
from sklearn.linear_model import LogisticRegression
from sklearn import preprocessing
from sklearn.ensemble import GradientBoostingClassifier, GradientBoostingRegressor


file_score = open("input/Score Train.csv")
file_book = open("input/Book Train.csv")
file_card = open("input/Card Train.csv")
file_library = open("input/Library Train.csv")
file_feature_train = open("input/QID2 Feature Train_110NUM_GBRTNULL_Lasso.dat","w")


LABEL_SEM = 3
SUBMIT = 0
WRITE = 1
LIMIT_MAX_ID = 91
book_date_splite_num = 4
book_date_splite = [15,30,60,120]
book_num = {}
book_num_lid = {}#id在第3维
book_write = 1
card_date_splite_num = 4
card_date_splite = [15,30,60,120]

card_day = {}
card_day_lid = {}#id在第3维
card_num = {}
card_num_lid = {}#id在第3维
card_write = 1

card_day_am_time = {}
card_day_pm_time = {}
card_day_time_write = 1

library_date_splite_num = 4
library_date_splite = [15,30,60,120]
library_day = {}
library_day_lid = {}#id在第3维
lib_write = 1

learn_date_splite_num = 4
learn_date_splite = [15,30,60,120]
learn_day = {}
learn_day_lid = {}#id在第3维
learn_write = 1

score_num = {}
score_num_lid = {}
book_date_end = ["121","715","120"]
library_date_end = ["121","715","120"]
card_date_end = ["121","715","120"]
###
MaxId = 0
####
##缺失值填充
####
nullfill = []

def DateToInt(d1,d2,sem):
    D1 = datetime.datetime(2005, int(d1) / 100 , int(d1) % 100 )
    D2 = datetime.datetime(2006,int(d2) / 100 , int(d2) % 100 )
    if sem == 2:
        D2 = datetime.datetime(2005,int(d2) / 100 , int(d2) % 100 )
    if sem == 1 and int(d1) / 100 < 9:
        D1 = datetime.datetime(2006, int(d1) / 100 , int(d1) % 100 )
    if sem == 3 and int(d1) / 100 < 9:
        D1 = datetime.datetime(2006, int(d1) / 100 , int(d1) % 100 )
    return  (D2-D1).days + 1

def Extract_Book(file):
    class_dict = {}
    class_dict_num = {}
    max_num_class = ""
    max_num = 0
    book_class_file = open("input/book class.txt")
    first_line = 0
    for line in book_class_file:
        if first_line == 0:
            first_line = 1
            continue
        line_cur = line.strip("").split("\t")
        #print line_cur
        class_dict.setdefault(line_cur[0],line_cur[1])
        class_dict_num.setdefault(line_cur[1],0)
        class_dict_num[line_cur[1]] += 1
        if class_dict_num[line_cur[1]] > max_num:
            max_num = class_dict_num[line_cur[1]]
            max_num_class = line_cur[1]
    print max_num_class
    first_line = 0
    for line in file:
        if first_line == 0:
            first_line = 1
            continue
        line_cur = line.strip("").split(",")
        sem = int(line_cur[0])
        id = int(line_cur[1])
        bookname = line_cur[2]
        if bookname not in class_dict or class_dict[bookname] != max_num_class:
            continue
        date = line_cur[3]
        intdate = DateToInt(date,card_date_end[sem-1],sem)#离最后一天的天数
        #print date + "\t" + str(intdate)
        book_num.setdefault(id,{})
        book_num[id].setdefault(sem,{})
        for idx in book_date_splite:
            if intdate < idx:
                book_num[id][sem].setdefault(idx,0.0)
                book_num[id][sem][idx] += 1.0
        book_num_lid.setdefault(sem,{})
        for idx in book_date_splite:
            book_num_lid[sem].setdefault(idx,{})
            if intdate < idx:
                book_num_lid[sem][idx].setdefault(id,0.0)
                book_num_lid[sem][idx][id] += 1.0
        global MaxId
        if id > MaxId:
            MaxId = id

def Extract_Library(file):
    first_line = 0
    for line in file:
        if first_line == 0:
            first_line = 1
            continue
        line_cur = line.strip("").split(",")
        sem = int(line_cur[0])
        id = int(line_cur[1])
        date = line_cur[2]
        intdate = DateToInt(date,card_date_end[sem-1],sem)#离最后一天的天数
        #print date + "\t" + str(intdate)
        library_day.setdefault(id,{})
        library_day[id].setdefault(sem,{})
        learn_day.setdefault(id,{})
        learn_day[id].setdefault(sem,{})
        for idx in library_date_splite:
            if intdate < idx:
                library_day[id][sem].setdefault(idx,{})
                library_day[id][sem][idx][date] = 0.0

                learn_day[id][sem].setdefault(idx,{})
                learn_day[id][sem][idx][date] = 0.0
                
        library_day_lid.setdefault(sem,{})
        for idx in library_date_splite:
            library_day_lid[sem].setdefault(idx,{})
            if intdate < idx:
                library_day_lid[sem][idx].setdefault(id,0.0)
                library_day_lid[sem][idx][id] += 1     
        learn_day_lid.setdefault(sem,{})
        for idx in library_date_splite:
            learn_day_lid[sem].setdefault(idx,{})
            if intdate < idx:
                learn_day_lid[sem][idx].setdefault(id,0.0)
                learn_day_lid[sem][idx][id] += 1
        global MaxId
        if id > MaxId:
            MaxId = id

def Extract_Card(file):
    first_line = 0
    for line in file:
        line = line.decode("GBK")
        if first_line == 0:
            first_line = 1
            continue
        line_cur = line.strip("").split(",")
        sem = int(line_cur[0])
        id = int(line_cur[1])
        place = line_cur[2]
        #print place
        date = line_cur[3]
        day_time = int(line_cur[4]) / 10000
        if day_time < 5 :day_time += 24
        if len(date) >= 5:
            continue
        intdate = DateToInt(date,card_date_end[sem-1],sem)#离最后一天的天数
        #print date + "\t" + str(intdate)
        if place != u"教室":
            continue
        if day_time <= 10:
            card_day_am_time.setdefault(id,{})
            card_day_am_time[id].setdefault(sem,{})
            card_day_am_time[id][sem].setdefault(intdate,{})
            card_day_am_time[id][sem][intdate][day_time] = 0.0
        if day_time >= 21:
            card_day_pm_time.setdefault(id,{})
            card_day_pm_time[id].setdefault(sem,{})
            card_day_pm_time[id][sem].setdefault(intdate,{})
            card_day_pm_time[id][sem][intdate][day_time] = 0.0


        card_day.setdefault(id,{})
        card_day[id].setdefault(sem,{})
        learn_day.setdefault(id,{})
        learn_day[id].setdefault(sem,{})
        for idx in card_date_splite:
            #print line,intdate,idx
            if intdate < idx:
                card_day[id][sem].setdefault(idx,{})
                card_day[id][sem][idx][date] = 0.0
                learn_day[id][sem].setdefault(idx,{})
                learn_day[id][sem][idx][date] = 0.0
                library_day_lid.setdefault(sem,{})
        learn_day_lid.setdefault(sem,{})
        card_day_lid.setdefault(sem,{})
        for idx in card_date_splite:
            learn_day_lid[sem].setdefault(idx,{})
            if intdate < idx:
                learn_day_lid[sem][idx].setdefault(id,0.0)
                learn_day_lid[sem][idx][id] += 1.0
            card_day_lid[sem].setdefault(idx,{})
            if intdate < idx:
                card_day_lid[sem][idx].setdefault(id,0.0)
                card_day_lid[sem][idx][id] += 1.0
        global MaxId
        if id > MaxId:
            MaxId = id

def Extract_Score(file):
    first_line = 0
    for line in file:
        if first_line == 0:
            first_line = 1
            continue
        line_cur = line.strip("\n").split(",")
        sem = int(line_cur[0])
        id = int(line_cur[1])
        rank = line_cur[2]
        score_num.setdefault(id,{})
        score_num[id].setdefault(sem,rank)
        score_num_lid.setdefault(sem,{})
        score_num_lid[sem].setdefault(id,rank)

def GetNullValue(f_dict,score_list):
    #print "original f_dict"
    #print f_dict
    cur_dict = f_dict
    x = []
    y = []
    temp = []
    cur_score_list = {}
    for id in range(1,92):
        temp.append((id,score_list[id]))
    temp.sort(lambda a,b :cmp(a[1],b[1]))
    for id in range(1,92):
        cur_score_list.setdefault(temp[id-1][0],id)
    for id in range(1,92):
        if id in f_dict:
            y.append(float(f_dict[id]))
            x.append([float(cur_score_list[id])])
    #print x
    #print y
    if len(x) < 1:
        return f_dict
    #clf =linear_model.LinearRegression()
    #clf = linear_model.Lasso(alpha = 0.03)
    #clf =  LogisticRegression(C=1.0, penalty='l1', tol=0.01)
    #clf = LogisticRegression()
    #clf = RandomForestClassifier(n_estimators=100,max_features=0.5)
    clf = GradientBoostingRegressor(n_estimators=30, max_depth=6, loss='ls')
    #clf = GradientBoostingClassifier(n_estimators=700, max_depth=6)
    #clf = svm.SVC()
    clf.fit(x,y)
    for id in range(1,92):
        if id not in f_dict:
            yy = clf.predict([cur_score_list[id]])
            f_dict.setdefault(id,yy[0])
    #print "current f_dict"
    #print f_dict
    return f_dict

def TransForm(sbid_d):
    idsb_d = {}
    for id in range(1,92):
        idsb_d.setdefault(id,{})
        for sem in range(1,3):
            idsb_d[id].setdefault(sem,{})
            for bsplit in book_date_splite:
                idsb_d[id][sem].setdefault(bsplit,sbid_d[sem][bsplit][id])
    return idsb_d

def FillNull():
    print "Fill Begin"
    if book_write == 1:
        for sem in range(1,3):
            for bsplit in book_date_splite:
                book_num_lid[sem][bsplit] = GetNullValue(book_num_lid[sem][bsplit],score_num_lid[sem])

    global book_num
    print "***********before**************"
    print book_num
    book_num = TransForm(book_num_lid)
    print "***********after**************"
    print book_num
    if lib_write == 1:
        for sem in range(1,3):
            for bsplit in book_date_splite:
                library_day_lid[sem][bsplit] = GetNullValue(library_day_lid[sem][bsplit],score_num_lid[sem])
    if card_write == 1:
        for sem in range(1,3):
            for bsplit in book_date_splite:
                # if sem in card_day_lid and bsplit in card_day_lid:
                    card_day_lid[sem][bsplit] = GetNullValue(card_day_lid[sem][bsplit],score_num_lid[sem])
    if learn_write == 1:
       for sem in range(1,3):
            for bsplit in book_date_splite:
                learn_day_lid[sem][bsplit] = GetNullValue(learn_day_lid[sem][bsplit],score_num_lid[sem])

if __name__ == '__main__':
    if book_write == 1:
        Extract_Book(file_book)
    if lib_write == 1:
        Extract_Library(file_library)
    if card_write == 1:
        Extract_Card(file_card)
    Extract_Score(file_score)
    print "max ID is " + str(LIMIT_MAX_ID)
    if WRITE == 1:
        #FillNull()
        qid_range = [1,100]
        for qid_i in range(0,1):
            for id1 in range(qid_range[qid_i],qid_range[qid_i+1]+1):
               for id2 in range(qid_range[qid_i],qid_range[qid_i+1]+1):
                    if id1 != id2:
                        w_str = str(id1) + ',' + str(id2)
                        if book_write == 1:
                            for sem in range(1,4):
                                for bsplit in book_date_splite:
                                    if id1 in book_num and sem in book_num[id1]\
                                            and bsplit in book_num[id1][sem]:
                                        w_str += ',' + str(book_num[id1][sem][bsplit])
                                    else:
                                        w_str += ',0'
                                    if id2 in book_num and sem in book_num[id2]\
                                            and bsplit in book_num[id2][sem]:
                                        w_str += ',' + str(book_num[id2][sem][bsplit])
                                    else:
                                        w_str += ',0'
                        if lib_write == 1:
                            for sem in range(1,4):
                                for bsplit in book_date_splite:
                                    if id1 in library_day and sem in library_day[id1]\
                                            and bsplit in library_day[id1][sem]:
                                        w_str += ',' + str(len(library_day[id1][sem][bsplit]))
                                    else:
                                        w_str += ',0'
                                    if id2 in library_day and sem in library_day[id2]\
                                            and bsplit in library_day[id2][sem]:
                                        w_str += ',' + str(len(library_day[id2][sem][bsplit]))
                                    else:
                                        w_str += ',0'
                        if card_write == 1:
                            for sem in range(1,4):
                                for bsplit in book_date_splite:
                                    if id1 in card_day and sem in card_day[id1]\
                                            and bsplit in card_day[id1][sem]:
                                        w_str += ',' + str(len(card_day[id1][sem][bsplit]))
                                    else:
                                        w_str += ',0'
                                    if id2 in card_day and sem in card_day[id2]\
                                            and bsplit in card_day[id2][sem]:
                                        w_str += ',' + str(len(card_day[id2][sem][bsplit]))
                                    else:
                                        w_str += ',0'
                        if card_day_time_write == 1:
                            for sem in range(1,4):
                                #print "***********"
                                #起床时间，最早
                                card_day_min_time = []
                                if id1 not in card_day_am_time or sem not in card_day_am_time[id1]:
                                    w_str += ',0'
                                else:
                                    for one_day in card_day_am_time[id1][sem].items():#day:{}
                                        one_day_am_time = sorted(one_day[1].items(),key = lambda t:t[0])
                                        card_day_min_time.append(one_day_am_time[0][0])
                                    w_str += ',' + str(1.0*sum(card_day_min_time)/len(card_day_min_time))
                                card_day_min_time = []
                                if id2 not in card_day_am_time or sem not in card_day_am_time[id2]:
                                    w_str += ',0'
                                else:
                                    for one_day in card_day_am_time[id2][sem].items():#day:{}
                                        one_day_am_time = sorted(one_day[1].items(),key = lambda t:t[0])
                                        card_day_min_time.append(one_day_am_time[0][0])
                                    w_str += ',' + str(1.0*sum(card_day_min_time)/len(card_day_min_time))
                                    #print card_day_min_time
                                    #print "min"+str(1.0*sum(card_day_min_time)/len(card_day_min_time))
                                #晚归时间
                                card_day_max_time = []
                                if id1 not in card_day_pm_time or sem not in card_day_pm_time[id1]:
                                    w_str += ',0'
                                else:
                                    for one_day in card_day_pm_time[id1][sem].items():#day:{}
                                        one_day_pm_time = sorted(one_day[1].items(),key = lambda t:t[0],reverse=True)
                                        card_day_max_time.append(one_day_pm_time[0][0])
                                    w_str += ',' + str(1.0*sum(card_day_max_time)/len(card_day_max_time))
                                card_day_max_time = []
                                if id2 not in card_day_pm_time or sem not in card_day_pm_time[id2]:
                                    w_str += ',0'
                                else:
                                    for one_day in card_day_pm_time[id2][sem].items():#day:{}
                                        one_day_pm_time = sorted(one_day[1].items(),key = lambda t:t[0],reverse=True)
                                        card_day_max_time.append(one_day_pm_time[0][0])
                                    w_str += ',' + str(1.0*sum(card_day_max_time)/len(card_day_max_time))
                                    #print card_day_max_time
                                    #print "max"+str(1.0*sum(card_day_max_time)/len(card_day_max_time))

                        if learn_write == 1:
                           for sem in range(1,4):
                                for bsplit in book_date_splite:
                                    if id1 in learn_day and sem in learn_day[id1]\
                                            and bsplit in learn_day[id1][sem]:
                                        w_str += ',' + str(len(learn_day[id1][sem][bsplit]))
                                    else:
                                        w_str += ',0'
                                    if id2 in learn_day and sem in learn_day[id2]\
                                            and bsplit in learn_day[id2][sem]:
                                        w_str += ',' + str(len(learn_day[id2][sem][bsplit]))
                                    else:
                                        w_str += ',0'
                        for sem in range(1,3):
                            if score_num[id1][sem] > score_num[id2][sem]:
                                w_str += ",1"
                            else:
                                w_str += ",0"
                            #w_str += ',' + str(score_num[id1][sem]) + ',' + str(score_num[id2][sem])
                        if score_num[id1][LABEL_SEM]>score_num[id2][LABEL_SEM]:
                            w_str += ',1'
                        else:
                            w_str += ',0'
                        w_str += '\n'
                        file_feature_train.write(w_str)
