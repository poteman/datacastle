# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
from sklearn.semi_supervised import label_propagation
from sklearn.datasets import make_circles
import pandas

fr=open('b2d.txt')
lenses=[inst.strip().split('\t') for inst in fr.readlines()]
# print(lenses)
X=[]
for row in lenses:
    c=[]
    for i in range(0,len(row)):
        b=float(row[i])
        c.append(b)
    X.append(c)
X=np.array(X)
print(X.shape)


n_samples=X.shape[0]  #case数目：需要更改
# X,y=make_circles(n_samples=n_samples,shuffle=False)
outer,inner=0,1
labels=-np.ones(n_samples)
# red:
labels[0]=outer
labels[2]=outer
labels[3]=outer
labels[4]=outer
labels[5]=outer
labels[120]=outer
labels[563]=outer
labels[815]=outer
labels[302]=outer
labels[429]=outer
labels[450]=outer
labels[539]=outer
labels[549]=outer
labels[52]=outer
labels[55]=outer
labels[833]=outer
labels[666]=outer
labels[667]=outer
labels[668]=outer
labels[669]=outer
labels[769]=outer

# blue:
labels[560]=inner
labels[600]=inner
labels[700]=inner
labels[300]=inner
labels[303]=inner
labels[309]=inner
labels[409]=inner
labels[419]=inner
labels[439]=inner
labels[749]=inner
labels[750]=inner
labels[760]=inner
labels[130]=inner
labels[53]=inner
labels[54]=inner
labels[57]=inner
labels[801]=inner
labels[803]=inner
labels[804]=inner
labels[805]=inner
labels[809]=inner
labels[816]=inner
labels[819]=inner
labels[834]=inner
labels[842]=inner



# print(labels)

###############################################################################
# Learn with LabelSpreading
label_spread=label_propagation.LabelSpreading(kernel='knn',alpha=1.0)
label_spread.fit(X,labels)
# print(label_spread)
###############################################################################
# Plot output labels
output_labels=label_spread.transduction_

plt.figure(figsize=(16,9))

plt.subplot(1,2,1)

plot_outet_labeled,=plt.plot(X[labels==outer,0],
                             X[labels==outer,1],'rs')
plot_unlabeled,=plt.plot(X[labels==-1,0],X[labels==-1,1],'g.')
plot_inner_labeled,=plt.plot(X[labels==inner,0],
                             X[labels==inner,1],'bs')
plt.legend((plot_outet_labeled,plot_inner_labeled,plot_unlabeled),
           ('Outer Labeled','Inner Labeled','Unlabeled'),'upper left',
           numpoints=1,shadow=False)

plt.subplot(1, 2, 2)
output_label_array = np.asarray(output_labels)
outer_numbers = np.where(output_label_array == outer)[0]
inner_numbers = np.where(output_label_array == inner)[0]
plot_outer, = plt.plot(X[outer_numbers, 0], X[outer_numbers, 1], 'rs')
plot_inner, = plt.plot(X[inner_numbers, 0], X[inner_numbers, 1], 'bs')
plt.legend((plot_outer, plot_inner), ('Outer Learned', 'Inner Learned'),
           'upper left', numpoints=1, shadow=False)
plt.title("Labels learned with Label Spreading (KNN)")

plt.subplots_adjust(left=0.07, bottom=0.07, right=0.93, top=0.92)
plt.show()

print(output_labels)


output_labels = pandas.DataFrame({
        "y": output_labels
    })

output_labels.to_csv('output_labels.csv',index=None)