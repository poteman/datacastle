# -*- coding: utf-8 -*-
import numpy as np
import pandas
import matplotlib.pyplot as plt
from sklearn.semi_supervised import label_propagation


train_x=pandas.read_csv("../input/train_x.csv")
train_x=train_x.drop(['uid'],axis=1)
train_unlabeled=pandas.read_csv("../input/train_unlabeled.csv")
train_unlabeled=train_unlabeled.drop(['uid'],axis=1)
# print(train_x.shape)
# print(train_unlabeled.shape)
frames=[train_x,train_unlabeled]
result=pandas.concat(frames)
# print(result.head(5))
# print(result.tail(5))
print(result.shape)
n_samples=result.shape[0]
# print(n_samples)

train_y=pandas.read_csv("../input/train_y.csv")
train_y=train_y['y']
# print(train_y.shape)

labels=pandas.DataFrame(-np.ones(n_samples-train_x.shape[0]))
frames_labels=[train_y,labels]
labels_result=pandas.concat(frames_labels)
# print(labels_result.head(10))
# print(labels_result.tail(5))
print(labels_result.shape)


label_spread=label_propagation.LabelSpreading(kernel='knn',alpha=1.0)
label_spread.fit(result,labels_result)

output_labels=label_spread.transduction_
output_labels = pandas.DataFrame({
        "y": output_labels
    })
output_labels.to_csv('output_labels.csv',index=None)
# print(output_labels)