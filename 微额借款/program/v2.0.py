# -*- coding:utf-8 -*-
import pandas
import sklearn
from sklearn.ensemble import RandomForestClassifier
from sklearn import svm
from sklearn.linear_model import LinearRegression
from sklearn import linear_model
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import AdaBoostRegressor


#################################################################################
# 训练用特征值
train_x=pandas.read_csv("train_x.csv")
# print(train_x.head(5))
# print(train_x.shape)

features_type=pandas.read_csv("features_type.csv")
# print(features_type.head(5))
# print(features_type["type"].unique())
features_type.loc[features_type["type"]=='numeric',"type1"]=1
features_type.loc[features_type["type"]=='category',"type1"]=0

# print(features_type)

train_x=train_x.iloc[:,1:]
# print(train_x.shape)
# print(train_x.head(3))


# print(features_type.columns)

n=features_type.shape[0]

predictors_numeric=[]
predictors_category=[]
for item in range(n):
    if features_type["type1"].iloc[item]==1:
        predictors_numeric.append(train_x.columns[item])
    else:
        predictors_category.append(train_x.columns[item])

print(predictors_numeric)
print(predictors_category)

n_numeric=len(predictors_numeric)
n_category=len(predictors_category)

train_x_numeric=train_x[predictors_numeric]

train_x_category=train_x[predictors_category]

# print(u'训练样本特征shape')
# print(train_x.shape)
# print(u'训练样本特征前3行')
# print(train_x.head(3))
#################################################################################





#################################################################################
# 训练用标签值
train_y=pandas.read_csv("train_y.csv")
train_y=train_y['y']
# print(train_y.columns[0])
# print(u'训练样本标签shape')
# print(train_y.shape)
# print(u'训练样本标签前3行')
# print(train_y.head(3))
########################################################################








#################################################################################
# 预测用特征值
test_x=pandas.read_csv("test_x.csv")
test_x=test_x.iloc[:,1:]
test_x_numeric=test_x[predictors_numeric]
test_x_category=test_x[predictors_category]
# print(u'预测样本特征shape')
# print(test_x.shape)
# print(u'预测样本特征前3行')
# print(test_x.head(3))







#####################################################################################################
# 模型训练
# alg_category = RandomForestClassifier(random_state=1, n_estimators=10, min_samples_split=2, min_samples_leaf=1)
alg_category = svm.SVC(kernel='rbf', class_weight={1: 100})

alg_numeric = LinearRegression()

# alg=svm.SVR()
# alg= linear_model.Ridge (alpha = .5)
# alg=AdaBoostRegressor(DecisionTreeRegressor(max_depth=4),
#                         n_estimators=1000)
# alg = svm.SVR(kernel='rbf',C=1e3,gamma=0.001)
# alg.fit(train_x.iloc[:,0:5],train_y)


alg_numeric.fit(train_x_numeric,train_y)
alg_category.fit(train_x_category,train_y)



#####################################################################################################


#####################################################################################################
# 预测
# prediction=alg.predict(test_x.iloc[:,0:5])
prediction_numeric=alg_numeric.predict(test_x_numeric)
print(prediction_numeric.shape)
print(prediction_numeric)
print(n_numeric)


prediction_category=alg_category.predict(test_x_category)
print(prediction_category.shape)
print(prediction_category)
print(n_category)

prediction=prediction_numeric+prediction_category*1.5


prediction=(prediction-min(prediction))/(max(prediction)-min(prediction))
print('prediction:----------------->>>>>>>>>>>>>>>>>>>>')
print(prediction.shape)
print(prediction[0:5])


test_x_uid=pandas.read_csv("test_x.csv")
submission = pandas.DataFrame({
        "uid": test_x_uid["uid"],
        "score": prediction
    })

submission.to_csv("test_y_category.csv", index=False)
