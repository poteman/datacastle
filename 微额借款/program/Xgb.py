import pandas as pd
from pandas import Series,DataFrame

# numpy, matplotlib, seaborn
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_style('whitegrid')


# machine learning
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC, LinearSVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
import xgboost as xgb



# get homesite & test csv files as a DataFrame
homesite_df = pd.read_csv("../input/train.csv")
test_df= pd.read_csv("../input/test.csv")

# preview the input
homesite_df.head()


homesite_df.info()
print("----------------------------")
test_df.info()


# drop unnecessary columns, these columns won't be useful in analysis and prediction
homesite_df = homesite_df.drop(['Id'], axis=1)






# customers purchased insurance plan

# Plot
sns.countplot(x="Response", data=homesite_df)






# fill NaN values

homesite_df.fillna(-1, inplace=True)
test_df.fillna(-1, inplace=True)


# There are some columns with non-numerical values(i.e. dtype='object'),
# So, We will create a corresponding unique numerical value for each non-numerical value in a column of training and testing set.

from sklearn import preprocessing

for f in homesite_df.columns:
    if homesite_df[f].dtype=='object':
        lbl = preprocessing.LabelEncoder()
        lbl.fit(np.unique(list(homesite_df[f].values) + list(test_df[f].values)))
        homesite_df[f] = lbl.transform(list(homesite_df[f].values))
        test_df[f] = lbl.transform(list(test_df[f].values))


# define training and testing sets

X_train = homesite_df.drop("Response",axis=1)
Y_train = homesite_df["Response"]
X_test  = test_df.drop("Id",axis=1).copy()


# Xgboost

params = {"objective": "binary:logistic"}

T_train_xgb = xgb.DMatrix(X_train, Y_train)
X_test_xgb  = xgb.DMatrix(X_test)

gbm = xgb.train(params, T_train_xgb, 20)
Y_pred = gbm.predict(X_test_xgb)



# Create submission

submission = pd.DataFrame()
submission["Id"]= test_df["Id"]
submission["score"] = Y_pred

submission.to_csv('homesite.csv', index=False)