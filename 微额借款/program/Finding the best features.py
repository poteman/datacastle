# -*- coding:utf-8 -*-
import pandas
import sklearn
import numpy as np
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn import svm
from sklearn.linear_model import LinearRegression
from sklearn import linear_model
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import AdaBoostRegressor
from sklearn.feature_selection import SelectKBest, f_classif

#################################################################################
# 训练用特征值
train_x=pandas.read_csv("train_x.csv")
# print(train_x.head(5))
# print(train_x.shape)

features_type=pandas.read_csv("features_type.csv")
# print(features_type.head(5))
# print(features_type["type"].unique())
features_type.loc[features_type["type"]=='numeric',"type1"]=1
features_type.loc[features_type["type"]=='category',"type1"]=0

# print(features_type.shape)

train_x=train_x.iloc[:,1:]
# print(train_x.shape)
# print(train_x.head(3))


# print(features_type.columns)

n=features_type.shape[0]

predictors=[]
for item in range(n):
    if features_type["type1"].iloc[item]==1:
        predictors.append(train_x.columns[item])
# print(predictors)
train_x=train_x[predictors]
# print(u'训练样本特征shape')
# print(train_x.shape)
# print(u'训练样本特征前3行')
# print(train_x.head(3))
#################################################################################





#################################################################################
# 训练用标签值
train_y=pandas.read_csv("train_y.csv")
train_y=train_y['y']
# print(train_y.columns[0])
# print(u'训练样本标签shape')
# print(train_y.shape)
# print(u'训练样本标签前3行')
# print(train_y.head(3))
########################################################################


selector = SelectKBest(f_classif, k=20)
selector.fit(train_x,train_y)

# Get the raw p-values for each feature, and transform from p-values into scores
scores = -np.log10(selector.pvalues_)


plt.bar(range(len(predictors)), scores)
plt.xticks(range(len(predictors)), predictors, rotation='vertical')
plt.show()


submission = pandas.DataFrame({
        "feature": train_x.columns,
        "score": scores
    })

submission.to_csv("feature_score.csv", index=False)
