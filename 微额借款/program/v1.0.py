# -*- coding:utf-8 -*-
import pandas
import sklearn
from sklearn.ensemble import RandomForestClassifier
from sklearn import svm
from sklearn.linear_model import LinearRegression
from sklearn import linear_model
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import AdaBoostRegressor
from sklearn.feature_selection import SelectKBest, f_classif

#################################################################################
# 训练用特征值
train_x=pandas.read_csv("train_x.csv")
# print(train_x.head(5))
# print(train_x.shape)

features_type=pandas.read_csv("features_type.csv")
# print(features_type.head(5))
# print(features_type["type"].unique())
features_type.loc[features_type["type"]=='numeric',"type1"]=1
features_type.loc[features_type["type"]=='category',"type1"]=0

# print(features_type.shape)

train_x=train_x.iloc[:,1:]
# print(train_x.shape)
# print(train_x.head(3))


# print(features_type.columns)

n=features_type.shape[0]

predictors=[]
for item in range(n):
    if features_type["type1"].iloc[item]==1:
        predictors.append(train_x.columns[item])
# print(predictors)
train_x=train_x[predictors]
# print(u'训练样本特征shape')
# print(train_x.shape)
# print(u'训练样本特征前3行')
# print(train_x.head(3))
#################################################################################





#################################################################################
# 训练用标签值
train_y=pandas.read_csv("train_y.csv")
train_y=train_y['y']
# print(train_y.columns[0])
# print(u'训练样本标签shape')
# print(train_y.shape)
# print(u'训练样本标签前3行')
# print(train_y.head(3))
########################################################################








#################################################################################
# 预测用特征值
test_x=pandas.read_csv("test_x.csv")
test_x=test_x.iloc[:,1:]
test_x=test_x[predictors]
# print(u'预测样本特征shape')
# print(test_x.shape)
# print(u'预测样本特征前3行')
# print(test_x.head(3))







#####################################################################################################
# 模型训练
# alg = RandomForestClassifier(random_state=1, n_estimators=10, min_samples_split=2, min_samples_leaf=1)
# alg = LinearRegression()
# alg=svm.SVR()
# alg= linear_model.Ridge (alpha = .5)
# alg=AdaBoostRegressor(DecisionTreeRegressor(max_depth=4),
#                         n_estimators=1000)
alg = svm.SVR(kernel='rbf',C=1e3,gamma=0.001)
# alg.fit(train_x.iloc[:,0:5],train_y)
alg.fit(train_x,train_y)



#####################################################################################################


#####################################################################################################
# 预测
# prediction=alg.predict(test_x.iloc[:,0:5])
prediction=alg.predict(test_x)
prediction=(prediction-min(prediction))/(max(prediction)-min(prediction))
print('prediction:----------------->>>>>>>>>>>>>>>>>>>>')
print(prediction.shape)
print(prediction[0:5])


test_x_uid=pandas.read_csv("test_x.csv")
submission = pandas.DataFrame({
        "uid": test_x_uid["uid"],
        "score": prediction
    })

submission.to_csv("test_y.csv", index=False)
